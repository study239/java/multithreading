package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

// https://java-online.ru/concurrent-future.xhtml
// https://www.baeldung.com/spring-boot-exit-codes

@Slf4j
@Component
public class Example2 extends RuntimeException implements ApplicationRunner, ExitCodeGenerator {
    private int exitCode = 0;
    private final List<FutureTask> taskList = new ArrayList<>();
    private final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

    @Override
    public void run(ApplicationArguments args) throws Exception {

        // Подготовка менеджера потоков
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setThreadNamePrefix("gp-load-thread");
        executor.initialize();

        for (var i = 0; i < 100; i++) {
            log.info("test {}", i);
            taskList.add(new FutureTask<String>(new LoadTask()));
            executor.execute(taskList.get(taskList.size() - 1));
        }

        while (executor.getActiveCount() > 0) {
            log.info("completed {}/{}, active {}", executor.getThreadPoolExecutor().getCompletedTaskCount(), executor.getThreadPoolExecutor().getTaskCount(), executor.getActiveCount());
            Thread.sleep(5 * 1000);
            checkTasksStatus();
        }
        executor.shutdown();

        checkTasksStatus();
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }

    private void checkTasksStatus() {
        // Проверяем как выполнились таски. Если хоть одна выполнилась с ошибкой, то завершаем работу с ошибкой
        taskList.forEach(futureTask -> {

            // Если таска еще не завершена, то пропускаем ее, так как .get блокирующая функция. Если ее вызвать, то она
            // будет ждать пока завершится процесс
            if (!futureTask.isDone()) {
                return;
            }

            try {
                log.info(String.valueOf(futureTask.get()));
            } catch (Exception e) {
                executor.destroy();
                e.printStackTrace();
                exitCode = 1;
                throw new RuntimeException("Some of threads finished with error.");
            }
        });
    }


    @RequiredArgsConstructor
    @Slf4j
    static class LoadTask implements Callable {

        @Override
        public Boolean call() {
            log.info("Execute");
            try {
                Integer sleepIntervalSec = getRandomNumber(1, 3);
                Thread.sleep(sleepIntervalSec * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Integer isError = getRandomNumber(1, 5);
            if (isError == 3) {
                Integer errMath = 10 / 0;
            }

            log.info("Finish");
            return true;
        }


        public Integer getRandomNumber(int min, int max) {
            return (int) ((Math.random() * (max - min)) + min);
        }
    }
}
