package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Slf4j
// @Component
public class Example implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {

        // Подготовка менеджера потоков
        var executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setThreadNamePrefix("gp-load-thread");
        executor.initialize();

        for (var i = 0; i < 100; i++) {
            log.info("test {}", i);
            executor.execute(new LoadTask());
        }

        while (executor.getActiveCount() > 0) {
            log.info("completed {}/{}, active {}", executor.getThreadPoolExecutor().getCompletedTaskCount(), executor.getThreadPoolExecutor().getTaskCount(), executor.getActiveCount());
            Thread.sleep(5 * 1000);
        }
        executor.shutdown();

    }


    @RequiredArgsConstructor
    @Slf4j
    static class LoadTask implements Runnable {

        @Override
        public void run() {
            log.info("Execute");
            try {
                Integer sleepIntervalSec = getRandomNumber(1, 3);
                Thread.sleep(sleepIntervalSec * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Integer isError = getRandomNumber(1, 5);
            if (isError == 3) {
                Integer errMath = 10 / 0;
            }


            log.info("Finish");
        }


        public Integer getRandomNumber(int min, int max) {
            return (int) ((Math.random() * (max - min)) + min);
        }
    }
}
